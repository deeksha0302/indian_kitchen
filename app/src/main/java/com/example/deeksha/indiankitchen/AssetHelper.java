package com.example.deeksha.indiankitchen;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class AssetHelper extends SQLiteAssetHelper {
    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "MyDatabase.db";
    private Context mContext;
    public static final String COLUMN_CATEGORY="Category";
    public static final String COLUMN_FAVOURITE="Favourite";
    public static final String TABLE_RECIPES="RECIPES";

    public AssetHelper(Context context) {
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
        mContext=context;
        setForcedUpgrade();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        setForcedUpgrade();
    }
}
