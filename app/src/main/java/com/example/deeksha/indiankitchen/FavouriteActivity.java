package com.example.deeksha.indiankitchen;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class FavouriteActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    List<Model> main_list;
    RecipeRecyclerViewAdapter mAdapter;
    public static final String CATEGORY_VEG = "vege";
    public static final String CATEGORY_NON_VEG = "non_veg";
    public static final String CATEGORY_EXTRA = "category_extra";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        android.content.Intent intent = getIntent();
        main_list = new ArrayList<>();
        recyclerView = findViewById(R.id.recycler);
        LayoutAnimationController animationController = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(animationController);

        //recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        main_list = getFavouriteRecipes();
        mAdapter = new RecipeRecyclerViewAdapter(main_list, getApplicationContext());

        recyclerView.setAdapter(mAdapter);

        showRecords();


    }


    private void showRecords() {
        SQLiteDatabase db = new AssetHelper(this).getReadableDatabase();
        ArrayList<String> nameList = new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT * FROM RECIPES ", null);

        if (cursor.moveToFirst()) {
            do {
                nameList.add(cursor.getString(0).trim() + " : ");
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        // Toast.makeText(this, nameList.toString(), Toast.LENGTH_LONG).show();
    }




    private List<Model> getFavouriteRecipes() {
        SQLiteDatabase db = new AssetHelper(this).getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM RECIPES WHERE " + AssetHelper.COLUMN_FAVOURITE + "=1", null);
        if (cursor.moveToFirst()) {
            do {
                int imageId = getResources().getIdentifier(cursor.getString(2), "drawable", getPackageName());
                main_list.add(new Model(imageId, cursor.getString(0).trim(), cursor.getString(1).trim()));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return main_list;
    }

    private List<Model> getRecipesBasedOnSearch(String searchString) {
        SQLiteDatabase db = new AssetHelper(this).getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * from RECIPES where Name like '" + searchString + "%'", null);
        if (cursor.moveToFirst()) {
            do {
                int imageId = getResources().getIdentifier(cursor.getString(2), "drawable", getPackageName());
                main_list.add(new Model(imageId, cursor.getString(0).trim(), cursor.getString(1).trim()));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return main_list;
    }
}