package com.example.deeksha.indiankitchen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class FirstActivity extends AppCompatActivity {
    private static final String LOG_TAG =
            FirstActivity.class.getSimpleName();
    TextView textview4;
    ImageView imageview3;
    Button button1, button2;
    Animation frombottom, fromtop, fromleft, fromright;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        button2 = (Button) findViewById(R.id.buttonNonVeg);
        frombottom = AnimationUtils.loadAnimation(this, R.anim.frombottom);
        button2.setAnimation(frombottom);
        button1 = (Button) findViewById(R.id.buttonVeg);
        fromleft = AnimationUtils.loadAnimation(this, R.anim.fromleft);
        button1.setAnimation(fromleft);
        textview4 = (TextView) findViewById(R.id.textView4);
        fromtop = AnimationUtils.loadAnimation(this, R.anim.fromtop);
        textview4.setAnimation(fromtop);
        imageview3 = (ImageView) findViewById(R.id.imageView3);
        fromright = AnimationUtils.loadAnimation(this, R.anim.fromright);
        imageview3.setAnimation(fromright);
        android.content.Intent intent = getIntent();
    }

    public void launchSecondActivity(View view) {
        Intent intent = new Intent(this, SecondActivity.class);
        int buttonID = view.getId();
        switch (buttonID) {
            case R.id.buttonVeg:
                intent.putExtra(SecondActivity.CATEGORY_EXTRA, SecondActivity.CATEGORY_VEG);
                break;
            case R.id.buttonNonVeg:
                intent.putExtra(SecondActivity.CATEGORY_EXTRA, SecondActivity.CATEGORY_NON_VEG);
                break;
            default:
        }
        startActivity(intent);
    }
}
