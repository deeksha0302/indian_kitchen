package com.example.deeksha.indiankitchen;

public class Model {
    int Image1;
    String dish_name;
    String mRecipe;

    public String getRecipe() {
        return mRecipe;
    }

    public void setRecipe(String mRecipe) {
        this.mRecipe = mRecipe;
    }

    public Model(int image1, String dish_name, String recipe) {
        Image1 = image1;
        this.dish_name = dish_name;
        mRecipe = recipe;
    }

    public int getImage1() {
        return Image1;
    }

    public void setImage1(int image1) {
        Image1 = image1;
    }

    public String getDish_name() {
        return dish_name;
    }

    public void setDish_name(String dish_name) {
        this.dish_name = dish_name;
    }

}
