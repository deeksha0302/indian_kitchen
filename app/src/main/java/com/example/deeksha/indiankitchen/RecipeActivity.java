package com.example.deeksha.indiankitchen;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.List;

public class RecipeActivity extends AppCompatActivity {
    private EditText mShareTextEditText;
    private static final String TAG = "RecipeActivity";
    public static final String DISH_NAME = "DISH_NAME";
    public static final String DISH_IMAGE = "DISH_IMAGE";
    public static final String RECIPE = "RECIPE";


    private ImageView mDishImageView;
    private TextView mDishNameTextView;
    private TextView mDishDescTextView;
    private String mRecipe;
    private String mDishName;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        mShareTextEditText = findViewById(R.id.menu_share);
        mDishImageView = findViewById(R.id.imageView);
        mDishNameTextView = findViewById(R.id.dishNameTextView);
        mDishDescTextView = findViewById(R.id.recipeDescTextView);


        mDishName = getIntent().getStringExtra(DISH_NAME);
        int imageDrawableID = getIntent().getIntExtra(DISH_IMAGE, R.drawable.butter_chicken);


        mRecipe = getIntent().getStringExtra(RECIPE);

        mRecipe = mRecipe.replaceAll("\"", "");


        mDishImageView.setImageResource(imageDrawableID);
        mDishNameTextView.setText(mDishName);


        mDishDescTextView.setText(mRecipe);


        Log.d(TAG, "onCreate:Started." + mDishName + ", " + imageDrawableID);

        setTitle(mDishName);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.recipe_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.my_favourite) {
            updateFavouriteList(mDishName);
            Toast.makeText(this, "Favourite", Toast.LENGTH_SHORT).show();
        }

        if (id == R.id.menu_share) {
            Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();

            {
                String mimeType = "text/plain";
                ShareCompat.IntentBuilder
                        .from(this)
                        .setType(mimeType)
                        .setChooserTitle(R.string.share_recipe)
                        .setText(mRecipe)
                        .startChooser();
            }


        }
        return super.onOptionsItemSelected(item);
    }


    private void updateFavouriteList(String favRecipe) {
        SQLiteDatabase db = new AssetHelper(this).getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(AssetHelper.COLUMN_FAVOURITE,1);
        db.update(AssetHelper.TABLE_RECIPES,contentValues," NAME='"+favRecipe+"'",null);
        db.close();
    }
}
