package com.example.deeksha.indiankitchen;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class RecipeRecyclerViewAdapter extends RecyclerView.Adapter<RecipeRecyclerViewAdapter.RecipeViewHolder> {
    private List<Model> mRecipeList;
    private Context mContext;

    public RecipeRecyclerViewAdapter(List<Model> recipeList, Context context) {
        mRecipeList = recipeList;
        mContext = context;
    }

    public void updateRecipes(List<Model> recipes) {
        mRecipeList=new ArrayList<>();
        mRecipeList.addAll(recipes);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.custom_list_items, viewGroup, false);
        return new RecipeViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeViewHolder recipeViewHolder, int position) {
        Model recipeItem = mRecipeList.get(position);
        recipeViewHolder.bindData(recipeItem);
    }

    @Override
    public int getItemCount() {
        return mRecipeList.size();
    }


    public class RecipeViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private ImageView imageView;
        private RecipeRecyclerViewAdapter mAdapter;
        private TextView recipeTextView;

        public RecipeViewHolder(@NonNull View itemView, final RecipeRecyclerViewAdapter adapter) {
            super(itemView);
            mAdapter = adapter;
            nameTextView = itemView.findViewById(R.id.name);
            imageView = itemView.findViewById(R.id.image);
            recipeTextView = itemView.findViewById(R.id.recipeDescTextView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, RecipeActivity.class);
                    Model recipeItem = mRecipeList.get(getLayoutPosition());
                    intent.putExtra(RecipeActivity.DISH_NAME, recipeItem.getDish_name());
                    intent.putExtra(RecipeActivity.DISH_IMAGE, recipeItem.getImage1());
                    intent.putExtra(RecipeActivity.RECIPE, recipeItem.getRecipe());

                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
            });
        }

        public void bindData(Model recipeItem) {
            imageView.setImageResource(recipeItem.getImage1());
            nameTextView.setText(recipeItem.getDish_name());
        }
    }
}
