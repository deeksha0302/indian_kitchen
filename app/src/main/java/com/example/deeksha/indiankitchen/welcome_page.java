package com.example.deeksha.indiankitchen;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class welcome_page extends AppCompatActivity {
    TextView textView;
    Button button;
    ImageView imageView2;
    Animation frombottom,fromtop,fromright;

     private static final String LOG_TAG =
            welcome_page.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_page);
        button=(Button)findViewById(R.id.button);
        frombottom= AnimationUtils.loadAnimation(this,R.anim.frombottom);
        button.setAnimation(frombottom);
        imageView2=(ImageView)findViewById(R.id.imageView2);
        fromtop= AnimationUtils.loadAnimation(this,R.anim.fromtop);
        imageView2.setAnimation(fromtop);
textView=(TextView)findViewById(R.id.textView);
fromright=AnimationUtils.loadAnimation(this,R.anim.fromright);
textView.setAnimation(fromright);

    }
    public void launchFirstActivity(android.view.View view) {
        android.util.Log.d(LOG_TAG, "Button clicked!");
        android.content.Intent intent = new android.content.Intent(this, FirstActivity.class);

startActivity(intent);}
}

